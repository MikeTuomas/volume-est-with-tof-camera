#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 12:45:14 2020

@author: mikael
"""
import numpy as np
import captureFrame as cf
import cv2 as cv

it = 43

while True:
    print('iter',it)
    valinta = input('paina enter uuden kuvan ottamiseksi tai lopeta: ')
    if valinta == "lopeta":
            break
    _, grayObject = cf.capture()
    gray = np.divide(grayObject.data[-1],np.max(grayObject.data[-1])/255)
    filename = 'calib'+str(it)+'.jpg'
    cv.imwrite(filename,gray)
    it = it+1

