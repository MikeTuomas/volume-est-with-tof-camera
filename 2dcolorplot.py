#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 10:03:36 2020

@author: mikael
"""

import numpy as np
import matplotlib.pyplot as plt
# This import registers the 3D projection, but is otherwise unused.
# import YksiFramePix
# import YksiFrameMet
# import time
import cv2 as cv
# from matplotlib import cm
# from mpl_toolkits.mplot3d import Axes3D

"lisää timerin"
# time.sleep(5)

" ottaa kuvat "
# imagedata = YksiFramePix.main()
# metridata, grayObject = YksiFrameMet.main()


# z_pix = imagedata.data[3]
# z_met = YksiFrameMet.ImageData.data
# graydata = YksiFrameMet.GrayData.data #16-bit
# z_met = metridata.data[3]
# graydata = grayObject.data[3]
conf = confidenceData.data[-1]
_,confmask = cv.threshold(np.float32(conf),0.02,1,cv.THRESH_BINARY_INV)
dst3 = cv.inpaint(rawImg[-2],np.uint8(confmask),5,cv.INPAINT_NS)
conf = cv.undistort(np.float32(conf), par.mtx,par.dist, None, par.newcameramtx)
conf = conf[y:y+h,x:x+w]
noise = noiseData.data[-1]
noise = cv.undistort(np.float32(noise), par.mtx,par.dist, None, par.newcameramtx)
noise = noise[y:y+h,x:x+w]
dst2 = cv.medianBlur(dst,3)
# test = np.flip(z_met,axis=0)
# test = np.flip(test)
# test = metridata.data[5] - z_pix #- metridata.data[4]
blurtest = cv.medianBlur(rawImg[-2],7)
diff = cv.absdiff(lastImg,newImg)
_,mask = cv.threshold(diff,0.02,1,cv.THRESH_BINARY)
kernelopen = np.ones((4,4),np.uint8)
kernelclose = np.ones((7,7),np.uint8)

openmask = cv.morphologyEx(mask,cv.MORPH_OPEN,kernelopen)
closemask = cv.morphologyEx(openmask,cv.MORPH_CLOSE,kernelclose)

erodemask = cv.morphologyEx(closemask,cv.MORPH_ERODE,kernelopen)
tulos = np.multiply(newImg,mask)
# z_pix = z_pix[15:171, 0:190]
z_met = newImg

# z_met = cv.medianBlur(newImg,7)
# graydata = grayimage[15:171, 0:192]

# blurtest = uusimask
graydata = mask
# graydata = dst2
# test = test[15:171, 0:190]
# z_pix = noblurImages[-1]
# z_pix = dst
z_pix = np.add(newImg,mask)
test = np.add(newImg,closemask)


"3d plotille akselit"
y = np.shape(z_met)[0]
x = np.shape(z_met)[1]
_y = np.arange(y)
_x = np.arange(x)
X, Y = np.meshgrid(_x, _y)


z_pix_min, z_pix_max = np.min(z_pix), np.max(z_pix)
z_met_min, z_met_max = np.min(z_met), np.max(z_met)
gray_min, gray_max = np.min(graydata), np.max(graydata)
z_test_min, z_test_max = np.min(test), np.max(test)

fig, axs = plt.subplots(2,2)

ax = axs[0,0]
c = ax.imshow(z_pix, cmap='terrain_r', vmin=z_pix_min, vmax=z_pix_max)
ax.set_title('z_pix')
ax.axis([X.min(), X.max(), Y.min(), Y.max()])
fig.colorbar(c, ax=ax)
ax.set_aspect(aspect='equal')
ax.yaxis.set_major_locator(plt.NullLocator())
ax.xaxis.set_major_locator(plt.NullLocator())

ax = axs[0,1]
c = ax.imshow(z_met, cmap='terrain_r', vmin=z_met_min, vmax=z_met_max)
ax.set_title('z_met')
ax.axis([X.min(), X.max(), Y.min(), Y.max()])
fig.colorbar(c, ax=ax, orientation='vertical')
ax.set_aspect(aspect='equal')
# ax.yaxis.set_major_locator(plt.NullLocator())
# ax.xaxis.set_major_locator(plt.NullLocator())

ax = axs[1,0]
c = ax.imshow(graydata, cmap='terrain_r', vmin=gray_min, vmax=gray_max)
ax.set_title('graydata')
ax.axis([X.min(), X.max(), Y.min(), Y.max()])
fig.colorbar(c, ax=ax, orientation='vertical')
ax.set_aspect(aspect='equal')
ax.yaxis.set_major_locator(plt.NullLocator())
ax.xaxis.set_major_locator(plt.NullLocator())

ax = axs[1,1]
c = ax.imshow(test, cmap='terrain_r', vmin=z_test_min, vmax=z_test_max)
ax.set_title('test')
ax.axis([X.min(), X.max(), Y.min(), Y.max()])
fig.colorbar(c, ax=ax, orientation='vertical')
ax.set_aspect(aspect='equal')
ax.yaxis.set_major_locator(plt.NullLocator())
ax.xaxis.set_major_locator(plt.NullLocator())


fig.tight_layout()
plt.show()

