#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 10:46:46 2020

@author: mikael
"""
import math
import numpy as np
import matplotlib.pyplot as plt

depth =  [100 + x for x in range(0,2400,50)] # [mm]

pixleveys = [(math.tan(math.radians(30)) * x) / 112 for x in depth]
pixkorkeus = [(math.tan(math.radians(22.5)) * x * 2) / 171 for x in depth]
pixpinta_ala = [pixkorkeus[x] * pixleveys[x] for x in range(len(depth))]
todellinentilavuus = np.multiply(pixpinta_ala,depth)
mitattutilavuus = np.multiply(pixpinta_ala,np.add(depth,100))
pixtilavuusero = np.multiply(np.divide(mitattutilavuus,todellinentilavuus),100)
# fig, axs = plt.subplots(2,0)

# ax = axs [1,]
plt.figure(1)
plt.plot(depth, pixpinta_ala, label= "Pinta-ala")
plt.legend(loc = "upper left")
plt.axis([min(depth), max(depth) + 100, min(pixpinta_ala), max(pixpinta_ala) + 10])
plt.xlabel("Syvyys [mm]")
plt.ylabel("Pinta-ala [mm2]")
# plt.title("Yhden pikselin pinta-ala")
plt.grid(True)

plt.figure(2)
plt.plot(depth, pixleveys, label = "Pikselin leveys")
plt.plot(depth, pixkorkeus, label = "Pikselin korkeus")
plt.legend(loc = "upper left")
plt.text(max(depth) - 100, min(pixkorkeus) +1 , ("korkeus/leveys", round(pixkorkeus[1]/pixleveys[1],2)), ha='right')
plt.axis([min(depth), max(depth) + 100, min(pixkorkeus), max(pixkorkeus) + 2])
plt.xlabel("Syvyys [mm]")
plt.ylabel("Pituus [mm]")
# plt.title("Yhden pikselin sivun pituus")
plt.grid(True)
plt.show()

plt.figure(3)
plt.plot(depth, pixtilavuusero, label= "tilavuuden ero todelliseen tilavuuteen")
# plt.legend(loc = "upper left")
plt.axis([min(depth), max(depth) + 100, 100, max(pixtilavuusero)])
plt.xlabel("Syvyys [mm]")
plt.ylabel("%")
plt.title("Mitatun tilavuuden ero todelliseen tilavuuteen syvyys+100mm")
plt.grid(True)
