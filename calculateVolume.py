#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 09:48:12 2020

@author: mikael
"""
import numpy as np
import captureFrame as cf
from datetime import datetime
import math
import cv2 as cv
from types import SimpleNamespace
import json
from pathlib import Path
import matplotlib.pyplot as plt

def matVolume(mat):
    piclev = np.divide(np.multiply(mat,math.tan(math.radians(30))),112)
    pickor = np.divide(np.multiply(mat,math.tan(math.radians(22.5))),85.5)
    picarea = np.multiply(piclev,pickor)
    picvolume = np.multiply(mat,picarea)
    return picvolume, picarea, piclev, pickor                 
                     
def main():
    'ladataan undistortion parameterit'
    with open('undistparameters.json') as data_file:
        data_loaded = json.load(data_file)
    par = SimpleNamespace(**data_loaded)
    par.mtx = np.array(par.mtx)
    par.dist = np.array(par.dist)
    par.newcameramtx = np.array(par.newcameramtx)
    
    'crop parameters'
    x, y, w, h = par.roi
    # cropleft = 2
    # cropright = 192 #192
    # cropdown = 6 #15
    # cropup = 164 #171
    # if x < cropleft:
    #     x = cropleft
    # if y < cropdown:
    #     y = cropdown
    # if (x+w) > cropright:
    #     w = cropright-x
    # if (y+h) > cropup:
    #     h = cropup-y
    
    ' ota ensimmäinen kuva '
    data, grayObject, confidenceData, noiseData = cf.capture()
    'virheiden korjaus'
    conf = confidenceData.data[-1]
    _,confmask = cv.threshold(np.float32(conf),0.02,1,cv.THRESH_BINARY_INV)
    img = cv.inpaint(np.float32(data.data[-1]),np.uint8(confmask),5,cv.INPAINT_NS)
    'blurraus'
    img = cv.medianBlur(img,5)
    'undistort ja rajaus'
    lastImg = cv.undistort(img, par.mtx,par.dist, None, par.newcameramtx)
    lastImg = lastImg[y:y+h,x:x+w]
    'korjaus kerroin'
    # lastImg = np.multiply(lastImg,0.93)
    
    cumulativeKuvaV = []
    plaincumulativeKuvaV = []
    rawImg = []
    rawImg.append(np.float32(data.data[-1]))
    
    while True:
        toiminta = input("Ota seuraava kuva painamalla Enter tai kirjoita Lopeta: ")
        if toiminta == "Lopeta" or toiminta == 'lopeta':
            tallennus = input('Syötä kyllä tai k tallentaaksesi kuvat')
            if tallennus == 'kyllä' or tallennus == 'k':
                folder = 'seina1200Testi'
                now = datetime.now()
                current_time = now.strftime("%y_%m_%d_%H:%M:%S")
                print("Current Time =", current_time)
                foldername = folder + current_time
                pathlastImg = './' + foldername + '/lastImg.npy'
                pathgrayImg = './' + foldername + '/grayImg.npy'
                pathrawImg = './' + foldername + '/rawImg.npy'
                
                Path(foldername).mkdir(parents=True, exist_ok=True)
            
                np.save(pathlastImg,lastImg,allow_pickle=False)
                np.save(pathgrayImg,grayObject.data[-1],allow_pickle=False)
                np.save(pathrawImg,rawImg[-1],allow_pickle=False)
                
            # estää kaatumisen vaikkei mitään tallenneta
            try:
                errorvariable = newImg 
            except NameError:
                newImg = []
                previousImg = lastImg
                break
            break
        
        'kuvan otto'
        data, _, confidenseData, _ = cf.capture()
        'virheiden korjaus'
        conf = confidenceData.data[-1]
        _,confmask = cv.threshold(np.float32(conf),0.02,1,cv.THRESH_BINARY_INV)
        img = cv.inpaint(np.float32(data.data[-1]),np.uint8(confmask),5,cv.INPAINT_NS)
        'blurraus'
        img = cv.medianBlur(img,5)
        'undistort ja rajaus'
        newImg = cv.undistort(img, par.mtx,par.dist, None, par.newcameramtx) 
        newImg = newImg[y:y+h,x:x+w]
        
        'korjaus kerroin'
        # newImg = np.multiply(newImg,0.93)
        
        'maskaus'
        diff = cv.absdiff(lastImg,newImg)
        _,mask = cv.threshold(diff,0.015,1,cv.THRESH_BINARY)
        kernelopen = np.ones((4,4),np.uint8)
        kernelclose = np.ones((7,7),np.uint8)
        maskopen = cv.morphologyEx(mask,cv.MORPH_OPEN,kernelopen)
        maskclose = cv.morphologyEx(maskopen,cv.MORPH_CLOSE,kernelclose)
        # maskerode = cv.morphologyEx(maskclose,cv.MORPH_ERODE,kernelopen)
        maskednewImg = np.multiply(newImg,maskclose)
        plainmaskednewImg = np.multiply(newImg,mask)    
        
        'tilavuuden mittaus'
        maskednewV, maskednewA, newKor, newLev = matVolume(maskednewImg)
        maskedlastV = np.multiply(lastImg,maskednewA)
        deltaKuvaV = np.sum(maskedlastV) - np.sum(maskednewV)
        
        plainmaskednewV, plainmaskednewA, _,_ = matVolume(plainmaskednewImg)
        plainmaskedlastV = np.multiply(lastImg,plainmaskednewA)
        plaindeltaKuvaV = np.sum(plainmaskedlastV) - np.sum(plainmaskednewV)
        
        cumulativeKuvaV.append(deltaKuvaV)
        plaincumulativeKuvaV.append(plaindeltaKuvaV)
        
        print('syvyyskuvien erotusten tilavuus :', deltaKuvaV, 'litroissa', deltaKuvaV*1000, 'L')
        print('syvyyskuvien erotusten tilavuus plain mask:', plaindeltaKuvaV, 'litroissa', plaindeltaKuvaV*1000, 'L')
        # print('kumulatiivinen:',np.sum(cumulativeKuvaV)*1000,'L')
        # print('plain kumulatiivinen:',np.sum(plaincumulativeKuvaV)*1000,'L')
        
        'visualisaatio'
        maskfit = np.multiply(maskclose,diff)
        imglist = [[lastImg,'Ensimmäinen kuva'], [newImg,'Toinen kuva'], [maskclose,'maski'],[maskfit,'maskin osuvuus ja kuvien erotus']]
        count = 1
        for it in imglist:
            # if count == 1:
            #     cmap = 'Greys'
            # else:
            cmap = 'terrain_r'
            
            img_min, img_max = np.min(it[0]), np.max(it[0])
            plt.figure(count)
            plt.imshow(it[0],cmap=cmap, vmin=img_min, vmax=img_max)
            plt.title(it[1])
            
            ax = plt.gca()
            im = ax.images
            if im[0].colorbar is None:
                plt.colorbar(orientation='vertical') 
                
            plt.tight_layout()
            plt.show(block = False)
            plt.draw()
            count = count + 1
            plt.pause(1)

        rawImg.append(np.float32(data.data[-1])) 
        previousImg = lastImg
        # lastImg = newImg #tämä kun on kommentoitu ohjelma vertaa aina ensimmäisenä otettuun kuvaan
        
    return previousImg,newImg,grayObject.data[-1],rawImg,confidenceData,noiseData

if (__name__ == "__main__"):
    lastImg,newImg,grayimage,rawImg,confidenceData, noiseData = main()
    
    
    
