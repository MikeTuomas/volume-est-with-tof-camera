#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script reads a registered pair of color and depth images and generates a
colored 3D point cloud in the PLY format.
"""

import argparse
import sys
import os
from PIL import Image
import numpy as np

focalLength = 209.8667
centerX = 104.633
centerY = 87.427
scalingFactor = 1

def generate_pointcloud(depth_file,ply_file):
    """
    Generate a colored point cloud in PLY format from a color and a depth image.
    
    Input:
    rgb_file -- filename of color image
    depth_file -- filename of depth image
    ply_file -- filename of ply file
    
    """
    # depth = Image.open(depth_file)
    
    depth = depth_file

    points = []
    it = np.nditer(depth, flags=['multi_index'])    
    for i in it:
        Z = depth[it.multi_index] / scalingFactor
        if Z==0: continue
        X = (it.multi_index[1] - centerX) * Z / focalLength
        Y = (it.multi_index[0] - centerY) * Z / focalLength
        points.append("%f %f %f \n"%(X,Y,Z))
    file = open(ply_file,"w")
    file.write('''ply
format ascii 1.0
element vertex %d
property float x
property float y
property float z
end_header
%s
'''%(len(points),"".join(points)))
    file.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
    This script reads a registered pair of color and depth images and generates a colored 3D point cloud in the
    PLY format. 
    ''')
    parser.add_argument('depth_file', help='input depth image (format: png)')
    parser.add_argument('ply_file', help='output PLY file (format: ply)')
    args = parser.parse_args()

    generate_pointcloud(args.depth_file,args.ply_file)

