#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 10:46:28 2020



@author: mikael
"""
import math

depth =  1000 # [mm]

pixleveys = (math.tan(math.radians(30))*depth)/112
pixkorkeus = (math.tan(math.radians(22.5))*depth*2)/171
pixpinta_ala = pixkorkeus*pixleveys
print("Syvyydessä",depth,"mm")
print("pikselin korkeus on", round(pixkorkeus,4), "mm")
print("pikselin leveys on", round(pixleveys,4), "mm")
print("pinta-ala:", round(pixpinta_ala,4), "mm2")
print("korkeus/leveys suhde:", round(pixkorkeus/pixleveys, 4))
