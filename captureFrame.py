#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 09:52:01 2020

@author: mikael
"""
import argparse
import roypy
import time
import queue
from sample_camera_info import print_camera_info
from roypy_sample_utils import CameraOpener, add_camera_opener_options

import numpy as np

class ImageData:
    def __init__(self):
        self.data = []
    def add (self, item):
        self.data.append(item)
                    
class MyListener(roypy.IDepthDataListener):
    def __init__(self, q, q1, q2, q3):
        super(MyListener, self).__init__()
        self.queue = q
        self.queue1 = q1
        self.queue2 = q2
        self.queue3 = q3
        
    def onNewData(self, data):
        zvalues = []
        grayvalues = []
        confidencevalues = []
        noisevalues = []
        
        for i in range(data.getNumPoints()):
            zvalues.append(data.getZ(i))
            grayvalues.append(data.getGrayValue(i))
            confidencevalues.append(data.getDepthConfidence(i))
            noisevalues.append(data.getNoise(i))
            
        zarray = np.asarray(zvalues)
        grayarray = np.asarray(grayvalues)
        confidencearray = np.asarray(confidencevalues)
        noisearray = np.asarray(noisevalues)
        
        p = zarray.reshape (-1, data.width)
        p1 = grayarray.reshape (-1, data.width)
        p2 = confidencearray.reshape (-1, data.width)  
        p3 = noisearray.reshape(-1,data.width)
        self.queue.put(p)
        self.queue1.put(p1)
        self.queue2.put(p2)
        self.queue3.put(p3)

        
def process_event_queue (q, q1, q2, q3, painter, seconds, im, graydat, conf, noise) :
    # create a loop that will run for the given amount of time
    # t_end = time.time() + seconds
    count = 0
    # while count == 0:
    t_end = time.time() + seconds
    while time.time() < t_end:
        try:
            # try to retrieve an item from the queue.
            # this will block until an item can be retrieved
            # or the timeout of 1 second is hit
            item = q.get(True, 1)
            item1 = q1.get(True, 1)
            item2 = q2.get(True, 1)
            item3 = q3.get(True, 1)
        except queue.Empty:
            # this will be thrown when the timeout is hit
            print('breakki')
            break
        else:
            im.add(item)
            graydat.add(item1)
            conf.add(item2)
            noise.add(item3)
            count +=1

def capture ():
    parser = argparse.ArgumentParser (usage = __doc__)
    add_camera_opener_options (parser)
    parser.add_argument ("--seconds", type=int, default=1, help="duration to capture data (default is 1 second)")
    options = parser.parse_args()
    opener = CameraOpener (options)
    cam = opener.open_camera ()
    cam.setExposureMode(exposureMode = 1) # 1 automatic, 0 manual

    # cam.setExposureTime(400)
    cam.setFilterLevel(level = 2) # 2 full, 1 'legacy', 0 none
    
    # print_camera_info (cam)
    # print("isConnected", cam.isConnected())
    # print("getFrameRate", cam.getFrameRate())
    # print("ExposureMode:    " + str(cam.getExposureMode()))
    # print("filterlevel:" + str(cam.getFilterLevel()))
    im = ImageData()
    graydat = ImageData()
    confidenceData = ImageData()
    noiseData = ImageData()
    # we will use this queue to synchronize the callback with the main
    # thread, as drawing should happen in the main thread
    q = queue.Queue()
    q1 = queue.Queue()
    q2 = queue.Queue()
    q3 = queue.Queue()
    l = MyListener(q,q1,q2,q3)
    cam.registerDataListener(l)
    cam.startCapture()
    # create a loop that will run for a time (default 1 second)
    process_event_queue (q, q1, q2, q3, l, 3, im, graydat, confidenceData, noiseData)
    cam.stopCapture()
    return im, graydat, confidenceData, noiseData


