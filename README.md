This repository contains stuff used and created in my bachelor thesis project

[Link to the thesis (in finnish)](http://urn.fi/URN:NBN:fi:amk-2020060116066)


[Help](https://github.com/k5iogura/libroyale-LINUX-arm)

# Used SW/HW

TOF Camera: PMDtec Camboard Picoflexx  
OS: [Ubuntu 18 LTS](https://ubuntu.com/download/desktop)  
Python environment: [Anaconda](https://www.anaconda.com/distribution/)  

