#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 12:25:51 2020

@author: mikael
"""

import numpy as np
import matplotlib.pylab as plt
# This import registers the 3D projection, but is otherwise unused.
import YksiFramePix
import YksiFrameMet
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
# YksiFramePix.main()
# YksiFrameMet.main()


z_met = newImg

# y = np.linspace(0,170,171)
# x = np.linspace(0,223,224)
"3d plotille meshgrid"
y, x = np.shape(z_met)

_y = np.arange(y)
_x = np.arange(x)
X, Y = np.meshgrid(_x, _y)

# z_pix = -1*YksiFramePix.ImageData.data
# z_met = test
# z_pix_min, z_pix_max = np.min(z_pix), np.max(z_pix)
z_met_min, z_met_max = np.min(z_met), np.max(z_met)
# z_met = -z_met
# fig1 = plt.figure()
fig2 = plt.figure()
# ax1 = fig1.gca(projection='3d')
ax2 = fig2.gca(projection='3d')
# ax.contour3D(X, Y, z, 50, cmap='binary')


# ax1.plot_surface(X, Y, z_pix, cmap=cm.jet, rstride=5, cstride=5, shade=False)

ax2.plot_surface(X, Y, z_met, cmap=cm.jet, rstride=2, cstride=2, shade=False)

# ax1.set_title('Pixel')
# ax2.set_title('Pahvin palanen laatikossa')
# ax1.set_xlabel('x')
# ax1.set_ylabel('y')
# ax1.set_zlabel('z')
ax2.set_xlabel('x')
ax2.set_ylabel('y')
ax2.set_zlabel('z')

fig2.tight_layout()
plt.show()
