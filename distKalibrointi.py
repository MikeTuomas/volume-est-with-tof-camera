#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 10:34:58 2020

@author: mikael
tämän koodin tarkoitus on löytää objpoints ja imgpoints cv.calibrateCamera methodille

sijoita samaan kansioon kuvat joilla kalibraatio tehdään. 
Kuvissa tulee olla shakki lauta eri asennoissa.
vähintään kymmenestä kuvasta tulisi löytää kulmat, 
eli count listaan tulisi tulla vähintää 10 tiedostoa
"""
import numpy as np
import cv2 as cv
import glob
# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((4*5,3), np.float32)
objp[:,:2] = np.mgrid[0:5,0:4].T.reshape(-1,2)
objp = objp.reshape(-1,1,3)
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob('*.jpg')
count = []
for fname in images:
    img = cv.imread(fname)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (5,4), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        count.append(fname)
        objpoints.append(objp)
        imgpoints.append(corners)
        corners2 = cv.cornerSubPix(gray,corners, (9,9), (-1,-1), criteria)
        
        # Draw and display the corners
        cv.drawChessboardCorners(img, (5,4), corners2, ret)
        cv.imshow('img', img)
        cv.waitKey(4000)
        
# toiminta = input("Ota seuraava kuva painamalla Enter tai kirjoita Lopeta: ")
k = cv.waitKey(0) & 0xFF
if k == 27:
    cv.destroyAllWindows()


    
# ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints,imgpoints,gray.shape[::-1],None,None)

# img = cv.imread('calib12.jpg')
# h, w = img.shape[:2]
# newcameramtx, roi = cv.getOptimalNewCameraMatrix(par.mtx, par.dist, (w,h),1, (w,h))

# dst = cv. undistort(img, par.mtx,par.dist, None, par.newcameramtx)

# x, y, w, h = par.roi
# dst = dst[y:y+h, x:x+w]
# cv.imwrite('testi.png', dst)