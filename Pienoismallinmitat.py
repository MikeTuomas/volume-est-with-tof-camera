#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 15:28:23 2020

säiliön pienoismallin mittojen laskenta

mitat [mm]
@author: mikael
"""
# todelliset mitat
Rkorkeus = 2500
Rsivu = 1600

# laatikon mitat
Lsivu = 368 

# suhdeluku
suhdeluku = Lsivu / Rsivu

# mallin korkeus 
Lkorkeus = suhdeluku * Rkorkeus


print("--Säiliön todelliset mitat--")
print("Kameran etäisyys pohjasta:               ", Rkorkeus,"mm")
print("Neliön muotoisen säiliön sivujen pituus: ", Rsivu,"mm")
print("\n --Pienoismallin mitat--")
print("Laatikon sivujen pituus määrittää kameran korkeuden.\n")
print("Laatikon sivun pituus on:                ", Lsivu,"mm")
print("Mallin suhdeluvuksi tulee:               ",round(suhdeluku,3))
print("Kameran korkeus mallissa:                ", Lkorkeus,"mm")