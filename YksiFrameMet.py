
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:21:53 2020

@author: mikael


"""

import argparse
import roypy
import time
import queue
from sample_camera_info import print_camera_info
from roypy_sample_utils import CameraOpener, add_camera_opener_options

import numpy as np
import matplotlib.pyplot as plt

class ImageData:
    def __init__(self):
        self.data = []
    def add (self, item):
        self.data.append(item)
                    
class MyListener(roypy.IDepthDataListener):
    def __init__(self, q, q1):
        super(MyListener, self).__init__()
        self.queue = q
        self.queue1 = q1
        
    def onNewData(self, data):
        zvalues = []
        grayvalues = []
        for i in range(data.getNumPoints()):
            zvalues.append(data.getZ(i))
            grayvalues.append(data.getGrayValue(i))
        zarray = np.asarray(zvalues)
        grayarray = np.asarray(grayvalues)
        p1 = grayarray.reshape (-1, data.width)
        p = zarray.reshape (-1, data.width)        
        self.queue.put(p)
        self.queue1.put(p1)


    def paint (self, data):
        """Called in the main thread, with data containing one of the items that was added to the
        queue in onNewData.
        """
        # create a figure and show the raw data
        plt.figure(2) #tää on varmaa paska tapa luoda figure, pitäs jotenkin dynaamisesti luoda...
        plt.imshow(data)
        plt.show(block = False)
        plt.draw()
        # this pause is needed to ensure the drawing for
        # some backends
        plt.pause(0.001)
        
def process_event_queue (q, q1, painter, seconds, im, graydat) :
    # create a loop that will run for the given amount of time
    t_end = time.time() + seconds
    while time.time() < t_end:
        try:
            # try to retrieve an item from the queue.
            # this will block until an item can be retrieved
            # or the timeout of 1 second is hit
            item = q.get(True, 1)
            item1 = q1.get(True, 1)
        except queue.Empty:
            # this will be thrown when the timeout is hit
            print('breakki')
            break
        else:
            # painter.paint (item)
            im.add(item)
            graydat.add(item1)

def main ():
    parser = argparse.ArgumentParser (usage = __doc__)
    add_camera_opener_options (parser)
    parser.add_argument ("--seconds", type=int, default=1, help="duration to capture data (default is 15 seconds)")
    options = parser.parse_args()
    opener = CameraOpener (options)
    cam = opener.open_camera ()
    cam.setExposureMode(exposureMode = 1)
    cam.setFilterLevel(level = 0)
    
    print_camera_info (cam)
    print("isConnected", cam.isConnected())
    print("getFrameRate", cam.getFrameRate())
    print("ExposureMode:    " + str(cam.getExposureMode()))
    print("filterlevel:" + str(cam.getFilterLevel()))
    im = ImageData()
    graydat = ImageData()
    # we will use this queue to synchronize the callback with the main
    # thread, as drawing should happen in the main thread
    q = queue.Queue()
    q1 = queue.Queue()
    l = MyListener(q,q1)
    cam.registerDataListener(l)
    cam.startCapture()
    # create a loop that will run for a time (default 15 seconds)
    process_event_queue (q, q1, l, options.seconds, im,graydat)
    cam.stopCapture()
    return im,graydat

